package room

import (
	"github.com/google/uuid"
	client "github.com/server/internal/client"
)

type Room struct {
	ID      uuid.UUID                    `json:"id"`
	Name    string                       `json:"name"`
	Clients map[uuid.UUID]*client.Client `json:"clients"`
}

func NewRoom(name string) *Room {
	return &Room{
		ID:      uuid.New(),
		Name:    name,
		Clients: make(map[uuid.UUID]*client.Client),
	}
}
