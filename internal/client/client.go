package client

import (
	"context"

	"github.com/google/uuid"
	"nhooyr.io/websocket"
)

type Client struct {
	ID   uuid.UUID `json:id"`
	Conn *websocket.Conn
	Ctx  context.Context
}

func NewClient(id uuid.UUID, conn *websocket.Conn) *Client {
	return &Client{
		ID:   id,
		Conn: conn,
		Ctx:  context.Background(),
	}
}
