module github.com/server

go 1.19

require (
	github.com/google/uuid v1.3.1
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/go-co-op/gocron v1.33.1 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	go.uber.org/atomic v1.9.0 // indirect
)
