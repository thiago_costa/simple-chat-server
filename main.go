package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/google/uuid"
	client "github.com/server/internal/client"
	room "github.com/server/internal/room"
	"nhooyr.io/websocket"
)

type RoomNode struct {
	Name string `json:"name"`
}

type RoomNodes struct {
	RoomNodes []RoomNode `json:"rooms"`
}

var (
	v_rooms   map[string]*room.Room        = make(map[string]*room.Room)
	v_clients map[uuid.UUID]*client.Client = make(map[uuid.UUID]*client.Client)
)

type RoomRes struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}

type Message struct {
	Content string    `json:"content"`
	RoomID  uuid.UUID `json:"room_id"`
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
	clientId := r.URL.Query().Get("clientId")
	conn, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		InsecureSkipVerify: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	newClient := *client.NewClient(uuid.MustParse(clientId), conn)

	v_clients[newClient.ID] = &newClient
}

func main() {
	fmt.Println("Ok")

	file, err := os.Open("./data.json")
	if err != nil {
		fmt.Println(err)
	}
	// defer file.Close()

	byteValue, _ := io.ReadAll(file)

	rooms := RoomNodes{}

	json.Unmarshal(byteValue, &rooms)

	for i := 0; i < len(rooms.RoomNodes); i++ {
		r := room.NewRoom(rooms.RoomNodes[i].Name)
		v_rooms[r.Name] = r
	}

	http.HandleFunc("/ws", wsHandler)

	http.HandleFunc("/rooms", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		w.Header().Add("Content-Type", "application/json")

		var res []*RoomRes
		for _, r := range v_rooms {
			res = append(res, &RoomRes{
				ID:   r.ID,
				Name: r.Name,
			})
		}

		jsonResp, err := json.Marshal(res)
		if err != nil {
			log.Fatal(err)
		}

		w.Write([]byte(jsonResp))
	})

	http.HandleFunc("/subscribe", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		roomId := uuid.MustParse(r.URL.Query().Get("roomId"))
		clientId := uuid.MustParse(r.URL.Query().Get("clientId"))

		// fmt.Printf("clientId: %v, roomId: %v", clientId, roomId)

		for _, r := range v_rooms {
			if r.ID == roomId {
				r.Clients[clientId] = v_clients[clientId]
			}
		}
	})

	http.HandleFunc("/subscriptions", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		clientId := uuid.MustParse(r.URL.Query().Get("clientId"))

		fmt.Print(clientId)

		test()
	})

	go broadcast()

	http.ListenAndServe(":9090", nil)

}

func test() {
	r := v_rooms["Room 6"]
	msg := &Message{
		Content: "OLAR MEU CHAPA",
		RoomID:  r.ID,
	}
	broadcastChan <- *msg
}

var broadcastChan chan Message = make(chan Message)

func broadcast() {
	for m := range broadcastChan {
		for _, r := range v_rooms {
			if len(r.Clients) > 0 && r.Name == "Room 6" || r.Name == "Room 4" {
				for _, c := range r.Clients {
					msg, _ := json.Marshal(m)
					c.Conn.Write(c.Ctx, websocket.MessageText, msg)
				}
			}
		}
	}
}
